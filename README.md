# Ejemplo LiveData

Esta aplicación muestra un ejemplo de cómo usar **LiveData** para almacenar los datos (en este caso un temporizador descendente) y no perderlos al cambiar la configuración de la Activity (ej. al girar la pantalla).

Consideraciones:



1. En el archivo _build.gradle_ se puede observar que se incluye una dependencia: <code>implementation <strong>"androidx.lifecycle:lifecycle-extensions:2.2.0"</strong></code>

2. Los datos que se quieran almacenar deben estar en una clase fuera de la Activity, esta clase debe ser heredera de <strong>ViewModel</strong>.

3. Los datos de esa clase comentada deberán estar “custodiados” por una clase MutableLiveData que utiliza una clase parametrizada para especificar el tipo del dato, ejemplo:
```
private MutableLiveData<Integer> seconds=new MutableLiveData<Integer>();

```


4. En esa clase, puede observarse que tendrémos un método get que solicita acceso a esa variable “mutable”, pero lo que se le entrega es otra clase parametrizada similar de tipo LiveData&lt;>, ejemplo:
```
public LiveData<Integer> getSecondsLiveData() { return seconds; }

```


5. En la Activity principal se usarán de forma clásica, con la única salvedad de que habrá que crear un observador que “vigile” las posibles modificaciones del valor de esa variable, y eso puede hacerse de la siguiente forma:
```
viewModel.getSecondsLiveData().observe(this, new Observer<Integer>() {
  @Override
  public void onChanged(Integer integer) {
     binding.tvNumero.setText(integer.toString());
  }
});
```


